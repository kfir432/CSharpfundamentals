﻿using System;
using System.Collections.Generic;

namespace CSharpfundamentals
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            
            //LIST
            /*
            var numbers = new List<int>(){1,2,3,4,5};

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }
            
            */
            
//            var dateTime = new DateTime();
//            var now = DateTime.Now;
//
//            Console.WriteLine(now.ToString());
            
            var numbers = new List<int>(){1,2,3,4,5,6};
            var smallestNumbers = GetSmallestNumbers(numbers, 3);

            foreach (var number in smallestNumbers)
            {
                Console.WriteLine(number);
            }
            
            

        }

        public static List<int> GetSmallestNumbers(List<int> list,int count)
        {
            
            if(count > list.Count)
                throw new ArgumentOutOfRangeException("count","Count cannot be greater than the number elements in list");
            
            var buffer = new List<int>(list);
            var smallests = new List<int>();
            

            while (smallests.Count < count)
            {
                var min = GetSmallest(buffer);
                smallests.Add(min);
                buffer.Remove(min);
            }

            return smallests;
            
            
        }
        
        public static int GetSmallest(List<int> list)
        {
            // Assume the first number is the smallest
            var min = list[0];
            for (var i = 1; i < list.Count; i++)
            {
                if (list[i] < min)
                    min = list[i];
            }
            return min;
        }
    }
}